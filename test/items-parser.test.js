const expect = require('chai').expect;

const itemsParser = require('../actions/items-parser');

describe('// Items parser', function() {
  it('should return a promise', function(done) {
    const source = './inputs/the-mysterious-island.pdf';
    const response = itemsParser(source);
    expect(response).to.be.a('promise');
    done();
  });
});
