const PDFParser = require('pdf2json/pdfparser');

/**
 * For each piece received from the pdf parsing, sends a formatted item.
 * @param {Object} pdf PDF Data received from PDF2JSON.
 * @param {Function} handler Callback function given by the user.
 * @return {Promise<any>} Promise waiting for the end of the pdf reading.
 */
function processItems(pdf, handler) {
  return new Promise((resolve) => {
    let pageNumber = 0;
    for (let p = 0; p < pdf.formImage.Pages.length; p++) {
      let page = pdf.formImage.Pages[p];
      let number = ++pageNumber;
      for (let t = 0; t < page.Texts.length; t++) {
        let item = page.Texts[t];
        handler({
          text: decodeURIComponent(item.R[0].T),
          page: number,
          x: item.x,
          y: item.y,
        });
      }
    }
    resolve();
  });
}

module.exports.processItems = processItems;

/**
 * Loops through the items parsed by Pdf2JSON and calls processItems on them.
 * @param {string} pdfFilePath Path of the source file to parse.
 * @param {Function} itemHandler Callback function given by the user.
 * @return {Promise<any>} Promise waiting for the end of the pdf reading.
 */
function parseFileItems(pdfFilePath, itemHandler) {
  return new Promise((resolve, reject) => {
    itemHandler({file: {path: pdfFilePath}});
    let pdfParser = new PDFParser();
    pdfParser.loadPDF(pdfFilePath, 0);

    pdfParser.on('pdfParser_dataError', (err) => {
      reject(
          {
            error: err,
            message:
`There was an issue reading the pdf file, please make sure the input file is 
correct.`,
          });
    });
    pdfParser.on('pdfParser_dataReady', (pdfData) => {
      processItems(pdfData, itemHandler)
          .then(() => {
            resolve();
          })
          .catch((err) => {
            console.error(err);
            reject(err);
          });
    });
  });
}

module.exports.parseFileItems = parseFileItems;
