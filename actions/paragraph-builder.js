const Paragraph = require('../models/paragraph');

/**
 * Comparison function for the items parsed by Pdf2JSON.
 * @param {Object} itemA
 * @param {Object} itemB
 * @return {number} -1, 0 or 1 depending on the comparison result.
 */
function compareFn(itemA, itemB) {
  if (itemA.page < itemB.page) {
    return -1;
  }
  if (itemA.page > itemB.page) {
    return 1;
  }
  if (itemA.y < itemB.y) {
    return -1;
  }
  if (itemA.y > itemB.y) {
    return 1;
  }
  if (itemA.x < itemB.x) {
    return -1;
  }
  if (itemA.x > itemB.x) {
    return 1;
  }
  return 0;
}

module.exports.compareFn = compareFn;

/**
 * Builds an array of paragraphs
 * @param {Array} items Parsed items from the Pdf2JSON parser.
 * @param {string} filter RegExp-like filter.
 * @param {number} breakFactor Multiplying factor for line break detection.
 * @return {Array}
 */
function buildParagraphs(items, filter, breakFactor) {
  const result = [];
  const sortedItems = items.sort(compareFn);

  let paragraph = new Paragraph(sortedItems[0].page);
  paragraph.appendText(sortedItems[0].text);
  let lastBreak = 2.0;

  for (let i = 1; i < sortedItems.length; i++) {
    const lastItem = sortedItems[i-1];
    const item = sortedItems[i];
    const currentBreak = Math.abs(item.y - lastItem.y);

    if (item.page !== lastItem.page || currentBreak > lastBreak*breakFactor) {
      // If there is a page change or a thick line break
      // Cleans up the paragraph then pushes it into the result array
      paragraph.setPage(lastItem.page);
      paragraph.cleanText(filter);
      if (paragraph.getText() !== '') result.push(paragraph);

      // Instantiates a new paragraph
      paragraph = new Paragraph(item.page);
      lastBreak = Math.abs(item.y - lastItem.y);
    } else if (item.y !== lastItem.y) {
      // Else if there is a line break
      paragraph.appendText('\n');
      lastBreak = Math.abs(item.y - lastItem.y);
    }
    paragraph.appendText(item.text);
  }
  if (paragraph.getText() !== '') result.push(paragraph);
  return result;
}

module.exports.buildParagraphs = buildParagraphs;
