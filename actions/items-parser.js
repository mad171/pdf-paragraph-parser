const parseFileItems = require('../util/pdf-parser').parseFileItems;

const buildParagraphs = require('./paragraph-builder').buildParagraphs;

module.exports =
  function(sourceFile, FILTER = '', BREAK_FACTOR = 1.2) {
    return new Promise((resolve) => {
      const pdfItems = [];
      parseFileItems(sourceFile, function(item) {
        if (item && item.text) {
          pdfItems.push({
            page: item.page,
            y: item.y,
            x: item.x,
            text: item.text,
          });
        }
      })
          .then(() => {
            const pdfParagraphs =
              buildParagraphs(pdfItems, FILTER, BREAK_FACTOR);
            resolve(pdfParagraphs);
          })
          .catch((err) => {
            console.error(err.message);
          });
    });
  };
