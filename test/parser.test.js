const expect = require('chai').expect;

const pdfParser = require('../util/pdf-parser');

describe('// PDF Parser', function() {
  const pdfData = require('./mocks/parsed-pdf');
  describe('Mock data', function() {
    it('should be formatted correctly', function(done) {
      expect(pdfData).to.have.property('formImage');
      expect(pdfData.formImage).to.have.property('Pages');
      expect(pdfData.formImage.Pages).to.be.an('array');
      expect(pdfData.formImage.Pages.length).to.equal(10);
      expect(pdfData.formImage.Pages[0]).to.have.property('Texts');
      done();
    });
  });
  describe('processItems', function() {
    it('should return a promise', function(done) {
      const handler = () => null;
      const response = pdfParser.processItems(pdfData, handler);
      expect(response).to.be.a('promise');
      done();
    });
    it('should recognize an item', function(done) {
      pdfParser.processItems(pdfData, (data) => {
        expect(data).to.have.all
            .keys('x', 'y', 'text', 'page');
        expect(data.x).to.be.a('number');
        expect(data.y).to.be.a('number');
        expect(data.page).to.be.a('number');
        expect(data.text).to.be.a('string');
      })
          .then(() => done());
    });
  });
  describe('parseFileItems', function() {
    it('should return a promise', function(done) {
      const handler = () => null;
      const source = './inputs/the-mysterious-island.pdf';
      const response = pdfParser.parseFileItems(source, handler);
      expect(response).to.be.a('promise');
      done();
    });
  });
});
