const expect = require('chai').expect;

const paragraphBuilder = require('../actions/paragraph-builder');

describe('// Paragraph Builder', function() {
  describe('compareFn', function() {
    const items = require('./mocks/items-to-sort');
    const sortedItems = items.sort(paragraphBuilder.compareFn);
    it('should sort through pages', function(done) {
      expect(sortedItems[0].page).to.equal(1);
      expect(sortedItems[2].page).to.equal(4);
      done();
    });

    it('should sort through x', function(done) {
      expect(sortedItems[4].x).to.equal(3.151);
      expect(sortedItems[5].x).to.equal(3.353);
      done();
    });

    it('should sort through y', function(done) {
      expect(sortedItems[2].y).to.equal(9.825);
      expect(sortedItems[3].y).to.equal(10.977);
      done();
    });
  });
  describe('buildParagraphs', function() {
    const items = require('./mocks/items');
    const paragraphs = paragraphBuilder.buildParagraphs(items, '', 1.1);
    it('should build paragraphs', function(done) {
      expect(paragraphs).to.be.an('array');
      expect(paragraphs.length).to.equal(4);
      expect(paragraphs[0]).to.have.all.keys('page', 'text');
      done();
    });
  });
});
