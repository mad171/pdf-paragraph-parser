/**
 * Module ready to be imported as a function which takes 3 parameters
 * @param {string} sourceFile Path to the source file.
 * @param {string} filter RegExp string which determines the characters
 * @param {number} breakFactor Multiplying factor for line break detection.
 * to be deleted in the output, e.g. '[•*]'.
 */
module.exports = require('./actions/items-parser');
