/**
 * Paragraph object
 */
class Paragraph {
  /**
   * Initalizes a paragraph with a page number and a text content.
   * @param {number} page
   */
  constructor(page) {
    this.page = page;
    this.text = '';
  }

  /**
   * Text getter.
   * @return {string}
   */
  getText() {
    return this.text;
  }

  /**
   * Page number setter.
   * @param {number} page
   */
  setPage(page) {
    this.page = page;
  }

  /**
   * Append text at the end of the text string
   * @param {string} text Text to append.
   */
  appendText(text) {
    this.text += text;
  }

  /**
   * Replaces undesired characters like bullet points and trim the text.
   * @param {string} filter RegExp-like filter.
   */
  cleanText(filter) {
    if (filter) this.text = this.text.replace(new RegExp(filter, 'g'), '');
    this.text = this.text.trim();
  }
}

module.exports = Paragraph;
